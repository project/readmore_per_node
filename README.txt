INTRODUCTION
------------

This module replaces the default "readmore" text with some custom text for
individual node. That custom text will be used in the node teaser view for the
readmore link instead of the default "readmore" text.
It will also override the ds readmore text.

INSTALLATION
------------

* Install as usual, see http://drupal.org/node/895232 for further information.

CONFIGURATION
-------------

* After enabling the module, You can enable this option for a content type on
content type edit page.
* Then on node form you will see a vertical tab for Readmore per node.
* You can add a custom text of your own which will be use instead of the default
"readmore" text in teaser view of the node.

RECOMMENDED MODULES
-------------------

 * Token (https://www.drupal.org/project/token)
